# 317 Client + OpenGL
This is a 317 Client with OpenGL ported from RuneLite. Compared to today's RuneLite, it's a bit outdated though!

---

**This was released on [Rune-Server](https://www.rune-server.ee/runescape-development/rs2-client/downloads/697648-317-opengl-ported-runelite.html) by [Spooky](https://www.rune-server.ee/members/spooky/).**


---

>  as the title says heres a 317 deob(NOTHING HAS BEEN TOUCHED OTHER THAN THE ADDITION OF OPENGL) with runelite gpu plugin
> ![Image Preview](https://i.imgur.com/4zUtw1q.png)
>
> Credits: \
> K4rn4ge
